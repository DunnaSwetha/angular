import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {
  
  products: any;

  constructor() {
    this.products = [
      {ProId: 20981, ProName:'Kurtas', ProPrice:1000, ProBrand:'Anouk'},
      {ProId: 20982, ProName:'Leggings',  ProPrice:500, ProBrand:'Prisma'},
      {ProId: 20983, ProName:'Jeans', ProPrice:1500, ProBrand:'DNMX'},
      {ProId: 20984, ProName:'Lehanga',  ProPrice:3000, ProBrand:'Avaasa'},
      {ProId: 20985, ProName:'Sarees', ProPrice:5000, ProBrand:'Meena'}
    ];
  }

  ngOnInit() {
  }

}
