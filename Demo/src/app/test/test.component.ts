import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

    id: number;
    name: string;
    avg: number;
    address: any;
    hobbies: any;
  constructor(){
    alert("constructor invoked");

    this.id=101;
    this.name='Swetha';
    this.avg=23;

    this.address={
      StreetNo:10/16,
      City:'Vizag',
      State:'Ap',
      pincode:532215
    };
    this.hobbies = ['sleepng','Eating','writing','gardening']

  }
 ngOnInit(){
  alert("ngOnInit invoked");

 }
}