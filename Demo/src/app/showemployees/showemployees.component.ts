import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {
  
  employees: any;

  constructor() {
    this.employees = [
      {empId: 101, empName:'swetha', salary:1212.12, gender:'female',   doj:'2018-11-15', country:'India',    emailId:'swetha@gmail.com', password:'123'},
      {empId: 102, empName:'rahul',  salary:2323.23, gender:'Male',   doj:'2017-10-16', country:'China',    emailId:'rahul@gmail.com',  password:'123'},
      {empId: 103, empName:'praveen', salary:3434.34, gender:'Male', doj:'2016-09-17', country:'USA',      emailId:'praveen@gmail.com', password:'123'},
      {empId: 104, empName:'yogi',  salary:4545.45, gender:'Male',   doj:'2015-08-18', country:'SriLanka', emailId:'yogi@gmail.com',  password:'123'},
      {empId: 105, empName:'mohan', salary:5656.56, gender:'Male',   doj:'2014-07-19', country:'Nepal',    emailId:'mohan@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
  }

  

}

